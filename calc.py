import numpy as np
import matplotlib.pyplot as plt
import latexoutput as lout
print("Using latexoutput, Version", lout.__version__)

"""comment out what you need"""
#import matplotlib as mpl
#mpl.rc('axes', {'formatter.useoffset': False, 'formatter.limits': [-5, 5]})
from scipy.optimize import curve_fit
#from scipy.stats import sem # standard error of mean
from uncertainties import ufloat, ufloat_fromstr
#import uncertainties.unumpy as unp
#from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
#import scipy.constants as const
#from scipy.interpolate import interp1d
#from scipy.integrate import simps, quad, trapz


H = 0
K = 1
L = 2

lambda_k1 = 1.54093
lambda_k2 = 1.54478
lambda_average = 1.5417

fcc = np.array([[1,1,1],
		[2,0,0],
		[2,2,0],
		[3,1,1],
		[2,2,2],
		[4,0,0],
		[3,3,1],
		[4,2,0],
		[4,2,2],
		[3,3,3],
		[3,3,3]])

bcc = np.array([[1,1,0],
		[2,0,0],
		[2,1,1],
		[2,2,0],
		[3,1,0],
		[2,2,2],
		[3,2,1],
		[4,0,0],
		[3,3,0],
		[4,1,1],
		[4,1,1]])

diamant = np.array([[1,1,1],
		[2,2,0],
		[3,1,1],
		[4,0,0],
		[3,3,1],
		[4,2,2],
		[3,3,3],
		[4,4,0],
		[5,3,1],
		[6,2,0],
		[6,2,0]])



r, lam = np.genfromtxt("data/probe6.txt", unpack = True)
theta = r * np.pi / 360
d = lam / (2 * np.sin(theta))
d_ratio = d[0] / d;
lout.latex_table(name="d",
	content=[r, theta, d],
	col_title="$r$, $\\theta$, $d$",
	col_unit="\\milli\\meter,,\\angstrom",
	fmt="3.1,1.2,1.2",
	caption="Dargestellt sind die gemessenen Radien $r$ der Reflexe und die daraus berechneten Winkel $\\theta$ und Netzebenenabstände $d$.")


fcc_hkl = fcc[:,H]*100 + fcc[:,K]*10 + fcc[:,L]
fcc_m = np.sqrt(fcc[:,H]**2 + fcc[:,K]**2 + fcc[:,L]**2)
m_ratio_fcc = fcc_m / fcc_m[0]

bcc_hkl = bcc[:,H]*100 + bcc[:,K]*10 + bcc[:,L]
bcc_m = np.sqrt(bcc[:,H]**2 + bcc[:,K]**2 + bcc[:,L]**2)
m_ratio_bcc = bcc_m / bcc_m[0]

diamant_hkl = diamant[:,H]*100 + diamant[:,K]*10 + diamant[:,L]
diamant_m = np.sqrt(diamant[:,H]**2 + diamant[:,K]**2 + diamant[:,L]**2)
m_ratio_diamant = diamant_m / diamant_m[0]

lout.latex_table(name="fcc",
	content=[fcc_hkl, fcc_m, m_ratio_fcc, d_ratio],
	col_title="hkl, $m$, $m_i/m_1$, $d_1/d_i$",
	fmt="3.0,1.2,1.2,1.2",
	caption="Berechnung der Verhältnisse für die fcc-Struktur.")

lout.latex_table(name="bcc",
	content=[bcc_hkl, bcc_m, m_ratio_bcc, d_ratio],
	col_title="hkl, $m$, $m_i/m_1$, $d_1/d_i$",
	fmt="3.0,1.2,1.2,1.2",
	caption="Berechnung der Verhältnisse für die bcc-Struktur.")

lout.latex_table(name="diamant",
	content=[diamant_hkl, diamant_m, m_ratio_diamant, d_ratio],
	col_title="hkl, $m$, $m_i/m_1$, $d_1/d_i$",
	fmt="3.0,1.2,1.2,1.2",
	caption="Berechnung der Verhältnisse für die diamant-Struktur.")


#struktur ist fcc!

a = d * fcc_m
cos2theta = np.cos(theta)**2

lout.latex_table(name="gitterkonstante",
	content=[d, cos2theta, a],
	col_title="$d$, $\\cos^2(\\theta)$, $a$",
	fmt="1.2,1.2,1.2",
	caption="Aufgetragen ist die berechnete Gitterkonstante $a$ gegen den Streuwinkel $\\theta$ für die Metallprobe.")

def f(x,a,b):
    return a*x + b

par, cov = curve_fit(f, cos2theta, a)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0],err[0])
fit_b = ufloat(par[1],err[1])
lout.latex_val(name="c", val=fit_a, unit="\\angstrom")
lout.latex_val(name="b", val=fit_b, unit="\\angstrom")
par_f = par

x_offset = (np.max(cos2theta)-np.min(cos2theta))*0.05
x_plot = np.linspace(np.min(cos2theta)-x_offset,np.max(cos2theta)+x_offset,100)

fig = plt.figure()

# Einfacher Plot
plt.plot(cos2theta,a,'rx',label='Messdaten')

# Plot einer Ausgleichs- oder Theoriefunktion f
plt.plot(x_plot,f(x_plot,*par_f),'b-',label='Fit')

plt.xlabel("$\\cos^2(\\theta)$")
plt.ylabel("$a$")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.legend(loc='best')
plt.savefig("build/gitterkonstante.pdf")
plt.close()





### SALZ2

r, lam = np.genfromtxt("data/salz2.txt", unpack = True)
theta = r * np.pi / 360
d = lam / (2 * np.sin(theta))

lout.latex_table(name="d_salz",
	content=[r, theta, d],
	col_title="$r$, $\\theta$, $d$",
	col_unit="\\milli\\meter,,\\angstrom",
	fmt="3.1,1.2,1.2",
	caption="Dargestellt sind die gemessenen Radien $r$ der Reflexe und die daraus berechneten Winkel $\\theta$ und Netzebenenabstände $d$.")

cscl = np.array([[1,0,0],
		[1,1,0],
		[1,1,1],
		[2,0,0],
		[2,1,0],
		[2,1,1],
		[2,2,0],
		[3,0,0],
		[3,1,0],
		[3,1,1],
		[2,2,2],
		[3,2,0],
		[3,2,1],
		[4,0,0],
		[4,1,0],
		[4,1,1],
		[3,3,1],
		[4,2,2],
		[5,0,0]])

steinsalz = np.array([[1,1,1],
		[2,0,0],
		[2,2,0],
		[3,1,1],
		[2,2,2],
		[4,0,0],
		[3,3,1],
		[4,2,0],
		[4,2,2],
		[3,3,3],
		[4,4,0],
		[5,3,1],
		[6,0,0],
		[6,2,0],
		[5,3,3],
		[6,2,2],
		[4,4,4],
		[7,1,1]])

cscl_hkl = cscl[:,H]*100 + cscl[:,K]*10 + cscl[:,L]
cscl_m = np.sqrt(cscl[:,H]**2 + cscl[:,K]**2 + cscl[:,L]**2)
a_array = np.empty((r.size, cscl_hkl.size))
for i in range(r.size):
	a_array[i] = d[i] * cscl_m

lout.latex_table(name="cscl",
	content=[cscl_hkl, cscl_m, *a_array],
	col_title="hkl, $m$, $a_1$, $a_2$, $a_3$, $a_4$, $a_5$, $a_6$, $a_7$, $a_8$, $a_9$, $a_{10}$, $a_{11}$, $a_{12}$, $a_{13}$",
	fmt="3.0,1.2,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1",
	caption="Berechnung der Gitterkonstanten $a$ für die Cäsium-Chlorid-Struktur zum jeweiligen Radiusmesswert.")

steinsalz_hkl = steinsalz[:,H]*100 + steinsalz[:,K]*10 + steinsalz[:,L]
steinsalz_m = np.sqrt(steinsalz[:,H]**2 + steinsalz[:,K]**2 + steinsalz[:,L]**2)
a_array = np.empty((r.size, steinsalz_hkl.size))
for i in range(r.size):
	a_array[i] = d[i] * steinsalz_m

lout.latex_table(name="steinsalz",
	content=[steinsalz_hkl, steinsalz_m, *a_array],
	col_title="hkl, $m$, $a_1$, $a_2$, $a_3$, $a_4$, $a_5$, $a_6$, $a_7$, $a_8$, $a_9$, $a_{10}$, $a_{11}$, $a_{12}$, $a_{13}$",
	fmt="3.0,1.2,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1",
	caption="Berechnung der Gitterkonstanten $a$ für die Steinsalz-Struktur und Flourid-Sturktur zum jeweiligen Radiusmesswert.")

##Flourid-Struktur vorhanden

a = d * steinsalz_m[[0, 2, 3, 5, 6, 8, 9, 10, 11, 13, 14, 16, 16]]
cos2theta = np.cos(theta)**2

lout.latex_table(name="gitterkonstante_salz",
	content=[d, cos2theta, a],
	col_title="$d$, $\\cos^2(\\theta)$, $a$",
	fmt="1.2,1.2,1.2",
	caption="Aufgetragen ist die berechnete Gitterkonstante $a$ gegen den Streuwinkel $\\theta$ für das Salz.")

def f(x,a,b):
    return a*x + b

par, cov = curve_fit(f, cos2theta, a)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0],err[0])
fit_b = ufloat(par[1],err[1])
lout.latex_val(name="c_salz", val=fit_a, unit="\\angstrom")
lout.latex_val(name="b_salz", val=fit_b, unit="\\angstrom")
par_f = par

x_offset = (np.max(cos2theta)-np.min(cos2theta))*0.05
x_plot = np.linspace(np.min(cos2theta)-x_offset,np.max(cos2theta)+x_offset,100)

fig = plt.figure()

# Einfacher Plot
plt.plot(cos2theta,a,'rx',label='Messdaten')

# Plot einer Ausgleichs- oder Theoriefunktion f
plt.plot(x_plot,f(x_plot,*par_f),'b-',label='Fit')

plt.xlabel("$\\cos^2(\\theta)$")
plt.ylabel("$a$")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.legend(loc='best')
plt.savefig("build/gitterkonstante_salz.pdf")
plt.close()