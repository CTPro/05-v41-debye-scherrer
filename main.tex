\input{header.tex}
\input{metadata.tex}
\input{shortcuts.tex}

\author{
  Tim Kallage    \\    tim.kallage@tu-dortmund.de \and
  Christian Geister \\ christian.geister@tu-dortmund.de
}
\title{(\Versuchsnummer)\\\Versuchstitel}
\date{Durchführung: \Versuchsdatum}

\begin{document}
\maketitle
\thispagestyle{empty}
\vfill
\tableofcontents
\newpage

%------------------------------------------------
\section{Motivation}
%------------------------------------------------
Um die Struktur von Kristallen auf atomarer Ebene zu untersuchen wird eine
Messtechnik benötigt, deren räumliches Auflösungsvermögen in der selben Größenordnung
liegt wie atomare Abstände. Elektronen, langsame Neutronen, sowie Röntgenstrahlen erfüllen
diese Voraussetzung.

Das Ziel dieses Versuchs ist es, die Struktur eines kristallinen Materials zu bestimmen.
Dies wird mit der Debye-Scherrer-Methode durchgeführt, indem die Winkelverteilung
von Röntgenstrahlung untersucht wird, nachdem diese am Kristallgitter gebeugt wurde.

%------------------------------------------------
\section{Theoretische Grundlagen}
%------------------------------------------------
\subsection{Kristallstrukturen}
%------------------------------
Eine Kristallstruktur kann vollständig durch ein Punktgitter beschrieben werden,
an dessen Gitterpunkten sich eine Basis befindet, die entweder aus einem einzelnen
Atom oder einer Atomgruppe besteht.
Das Punktgitter ist wiederum durch 3 Vektoren $\vec a, \vec b, \vec c$, die fundamentalen
Translationen, definiert. Jeder Gitterpunkt kann durch einen Vektor
\begin{equation}\label{eq:translationsvektor}
  \vec t = n_1 \vec a + n_2 \vec b + n_3 \vec c,
  \qquad n_1, n_2, n_3 \in \mathbb{N}
\end{equation}
beschrieben werden und jede Translation um $\vec t$ überführt das Gitter in sich selbst.
Die durch die Wahl der fundamentalen Translationen erzeugten Gitter können in 14
Gittertypen unterteilt werden, die sogenannten Bravais-Gitter, die sich durch ihre
Symmetrieeigenschaften voneinander unterscheiden.

Für diesen Versuch sind von diesen 14 Gittertypen nur die drei kubischen interessant:
Kubisch-primitiv (sc), kubisch-flächenzentriert (fcc), sowie kubisch-raumzentriert (bcc).
Kubische Gitter zeichnen sich dadurch aus, dass die fundamentalen Translationen
paarweise senkrecht aufeinander stehen und alle den gleichen Betrag besitzen.
Bei einem sc-Gitter besteht die Basis aus einem Atom, dadurch befindet sich in jeder
Würfelecke ein Atom. Die fcc-Struktur enthält dazu in jeder Würfeloberfläche ein weiteres
Atom, die bcc-Struktur in jedem Würfelzentrum.

Weitere kubische Kristallstrukturen können aus diesen drei Basisgittern zusammengesetzt werden.
Die Diamant- und Zinkblendestrukturen bestehen aus zwei fcc-Gittern, die um $\sfrac{1}{4}$
auf der Raumdiagonalen zueinander verschoben sind. Die beiden Gitter bestehen bei der
Diamantstruktur (Zinkblendestruktur) aus gleichen (verschiedenen) Atomsorten.
Sind die beiden fcc-Gitter mit unterschiedlichen Atome um \sfrac{1}{2} auf der
Raumdiagonalen gegeneinander verschoben, erhält man die Steinsalzstruktur, ersetzt man die
fcc- durch sc-Gitter, so wird die Struktur als Cäsiumchloridstruktur bezeichnet.
Die Fluorit-Struktur kann bei Verbindungen des Typs AB$_2$ auftreten. Sie ist der
Zinkblendenstruktur sehr ähnlich, enthält dazu jedoch noch ein weiteres fcc-Gitter, welches
um \sfrac{3}{4} auf der Raumdiagonalen verschoben ist.
In Tabelle \ref{tbl:Atompositionen} ist eine Übersicht der Atompositionen in der Elementarzelle
dargestellt.
\begin{table}
  \caption{Atompositionen in der Elementarzelle verschiedener kubischer Gitter
  in Einheiten von einem Viertel der Gitterkonstanten, $\sfrac a 4$.}
  \label{tbl:Atompositionen}
  \centering
  \begin{tabular}{lccccc}
  \toprule
  \textbf{Struktur} & \textbf{Atomart} & \multicolumn{4}{c}{\textbf{Position} [$\frac a 4$]} \\
  \midrule
  sc & A & $(0,0,0)$ \\
  fcc & A & $(0,0,0)$ & $(2,2,0)$ & $(2,0,2)$ & $(0,2,2)$ \\
  bcc & A & $(0,0,0)$ & $(2,2,2)$ \\
  \multirow{2}{*}{Diamant} & \multirow{2}{*}{A} &
      $(0,0,0)$ & $(2,2,0)$ & $(2,0,2)$ & $(0,2,2)$ \\
  & & $(1,1,1)$ & $(3,3,1)$ & $(3,1,3)$ & $(1,3,3)$ \\
  \multirow{2}{*}{Zinkblende} & A & $(0,0,0)$ & $(2,2,0)$ & $(2,0,2)$ & $(0,2,2)$ \\
  & B & $(1,1,1)$ & $(3,3,1)$ & $(3,1,3)$ & $(1,3,3)$ \\
  \multirow{2}{*}{Steinsalz} & A & $(0,0,0)$ & $(2,2,0)$ & $(2,0,2)$ & $(0,2,2)$ \\
  & B & $(2,2,2)$ & $(4,2,2)$ & $(4,2,4)$ & $(2,4,4)$ \\
  \multirow{2}{*}{Cäsiumchlorid} & A & $(0,0,0)$ \\
  & B & $(2,2,2)$ \\
  \multirow{3}{*}{Fluorit} & A & $(0,0,0)$ & $(2,2,0)$ & $(2,0,2)$ & $(0,2,2)$ \\
  & \multirow{2}{*}{B} & $(1,1,1)$ & $(3,3,1)$ & $(3,1,3)$ & $(1,3,3)$ \\
  & & $(3,3,3)$ & $(1,1,3)$ & $(1,3,1)$ & $(3,1,1)$ \\
  \bottomrule
  \end{tabular}
\end{table}
%----------------------
\subsection{Netzebenen}
%----------------------
Netzebenen werden durch die Lage von Kristallatomen aufgespannt.
Die Menge aller parallelen, äquidistanten Netzebenen wird als Netzebenenschar bezeichnet.
Zur Beschreibung einer Netzebene werden die sog. Miller-Indizes konstruiert.
Dazu wird zuerst ein Translationsvektor nach \eqref{eq:translationsvektor} bestimmt, welcher
senkrecht auf der Netzebene steht.
Die Koeffizienten dieses Vektors erhält man beispielsweise aus den Kehrwerten der
Schnittpunkte mit den Achsen, die von den fundamentalen Translationen
aufgespannt werden.
Die Koeffizienten $n_1, n_2, n_3$ werden dann auf
das kleinstmögliche Tripel teilerfremder, ganzer Zahlen $(h,k,l)$ erweitert.
Ein negativer Index wird durch einen Strich über dem Index gekennzeichnet.
Alle symmetrisch äquivalenten Ebenen werden durch die Notation $\{h,k,l\}$ dargestellt.
Den Netzebenenabstand $d$ einer kubischen Kristallstruktur lässt sich aus
\begin{equation}\label{eq:d}
  d = \frac{a}{\sqrt{h^2 + k^2 + l^2}}
\end{equation}
bestimmen.
%----------------------
\subsection{Beugung von Röntgenstrahlung an Kristallen}
%----------------------
Geladene Teilchen werden von dem elektromagnetischen Feld der Röntgenstrahlung zu Oszillationen
angeregt und emittieren wiederum ebenfalls elektromagnetische Wellen.
Daher kann die Wechselwirkung von Röntgenstrahlung als klassischer Streuprozess betrachtet werden.
Da die Intensität der gestreuten Strahlung, analog zum Hertz'schen Dipol, proportional zur
inversen quadratischen Masse $\frac{1}{m^2}$ ist, kann die Streuung an den Kernen der
Kristallatome vernachlässigt werden.
Es muss beachtet werden, dass die Elektronen in gebundenen Zuständen vorliegen.
Die Elektronenhülle des Atoms besitzt daher eine Ausdehnung, die vergleichbar ist
mit der Wellenlänge von Röntgenlicht. Das Röntgenlicht streut daher an der gesamten, ausgedehnten
Elektronenhülle und die gestreuten Wellen interferieren durch den entstehenden Phasenunterschied miteinander.
Die Streuintensität an einem Atom im Kristall $I_a$ ist daher verschieden zu jener an einem
Einzelelektron $I_e$.
Dies wird durch den Atomformfaktor $f$, mit
\begin{equation}
  f^2 = \frac{I_a}{I_e},
\end{equation}
beschrieben.

Die an unterschiedlichen Netzebenen gestreuten Röntgenstrahlen interferieren miteinander,
sodass nur unter bestimmten Winkeln ein Röntgenreflex durch konstruktive Interferenz entsteht.
Dies wird durch die bekannte Bragg-Bedingung
\begin{equation}\label{eq:Bragg}
  n\lambda = 2 d \sin\theta,\qquad n \in \mathbb N,
\end{equation}
mit dem Winkel $\theta$ zwischen Netzebene und dem Wellenvektor des einfallenden
Röntgenstrahls, beschrieben.
Es ist nun feststellbar, das nicht jede Netzebene einen Reflex erzeugt.
Dies liegt an der Interferenz von reflektierten Wellen an unterschiedlichen Atomen
der Elementarzelle und die Amplitude des reflektierten Röntgenstrahls kann
mit der Strukturamplitude
\begin{equation}
  S(hkl) = \sum_j f_j \exp(-2\pi i(x_jh + y_jk + z_jl))
\end{equation}
beschrieben werden. Hierbei wird über alle Atome in der Elementarzelle summiert, mit den
Atomformfaktoren $f_j$ und den Koordinaten $x_j,y_j,z_j$.
Hieraus ergeben sich Bedingungen an die Miller'schen Indizes für
nicht-verschwindende Strukturamplituden:
\begin{itemize}
  \item {\bf sc}: beliebiges $h,k,l$,
  \item {\bf fcc}: $h,k,l$ gerade oder ungerade,
  \item {\bf bcc}: $h+k+l$ gerade,
  \item {\bf Diamant}:
    \begin{itemize}
      \item $h,k,l$ gerade und $h+k+l = 4n$ mit $n\in \mathbb N$
      \item $h,k,l$ ungerade und $h+k+l = 4n+1$ mit $n \in \mathbb N$
    \end{itemize}
  \item {\bf Steinsalz}: $h,k,l$ gerade (starker Reflex) oder ungerade (schwach)
  \item {\bf Cäsiumchlorid}: $h+k+l$ gerade (stark) oder $h+k+l$ ungerade (schwach)
  \item {\bf Flourid}:
    \begin{itemize}
      \item $h,k,l$ gerade und $h+k+l=4n$ mit $n\in\mathbb N$ (stark)
      \item $h,k,l$ gerade und $h+k+l=4n+2$ mit $n\in\mathbb N$ (schwach)
      \item $h,k,l$ ungerade (mittelstark)
    \end{itemize}
\end{itemize}
%------------------------------------------------
\section{Aufbau und Durchführung}
%------------------------------------------------
Das Debye-Scherrer-Verfahren verwendet monochromatisches Röntgenlicht zur Bestimmung der
Kristallstruktur eines Materials.
Das Material (Probe) wird in einem Zylinder als Pulver in die Mitte eines Film-Rings gebracht,
vgl. Abb.~\ref{fig:Aufbau}, und dort einem kollimierten, intensiven Röntgenstrahl ausgesetzt.
Durch die Pulverform des Materials liegt im Prinzip jeder Streuwinkel vor und der
punktförmige Röntgenstrahl wird in mehrere Kegelmantel gebeugt und erzeugt kreisförmige
Interferenzmaxima auf dem Film. Ein Beugungswinkel $\theta$ erzeugt dabei einen Kegel
mit dem Öffnungswinkel $2\theta$.
\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{Debye-Scherrer-Aufbau}
  \caption{Prinzipieller Versuchsaufbau zur Herstellung einer
  Debye-Scherrer-Aufnahme.\cite{Anleitung}}
  \label{fig:Aufbau}
\end{figure}
Bei der Messung der Winkel treten allerdings systematische Fehler auf.
Zum einen absorbiert das Probenmaterial die einfallende Röntgenstrahlung beinahe völlig,
wodurch die Beugung nur am Mantel des Probenzylinders auftritt.
Nach Bradley und Jay\cite{BradleyJay} führt dies zu einer Überschätzung des Winkels,
besonders bei kleinen Streuwinkeln,
und die zu bestimmende Gitterkonstante muss um den Faktor
\begin{equation}
  \frac{\Delta a_{\mathrm A}}{a} = \frac{\rho}{2R}\left(1 - \frac{R}{F}\right)
  \frac{\cos^2\theta}{\theta}
\end{equation}
korrigiert werden. Hierbei ist $\rho$ der Probenradius,
$R = \SI{57,3}{\milli\meter}$ der Kameraradius und
$F = \SI{130}{\milli\meter}$ der Abstand der Probe zum Fokus.
Des Weiteren entsteht ein systematischer Fehler durch eine Verschiebung der Probenachse
zur Zylinderachse, die dadurch entsteht, dass die Probe sich nicht genau im Mittelpunkt
des Films befindet.
Die hierbei entstehende Korrektur kann ebenso positiv wie negativ sein und berechnet sich nach
\begin{equation}
  \frac{a_\mathrm{V}}{a} = \frac{v}{R} \cos^2\theta,
  \label{eqn:fehler}
\end{equation}
mit dem Abstand $v$ zwischen den Achsen.
Der zweite systematische Fehler dominiert die Messung in diesem Versuchsaufbau
(bis auf kleine Winkel), weshalb eine näherungsweise Proportionalität zwischen
$a(\theta)$ und $\cos^2\theta$ besteht.

Die Gitterkonstante wird mithilfe von \eqref{eq:d} und \eqref{eq:Bragg} bestimmt und gegen
$\cos^2\theta$ aufgetragen. Mittels einer linearen Regression kann dann
$a(\theta = \SI{90}{\degree})$ extrapoliert werden, was der besten Schätzung für die wahre
Gitterkonstante $a$ entspricht.

Relevant für diesen Versuch ist ebenfalls die Wellenlänge des verwendeten Röntgenlichts.
Zur Erzeugung wird eine Röntgenröhre mit Cu-Anode verwendet, deren Beschleunigungsspannung
bei \SI{40}{\kilo\volt} liegt.
Da in diesem Versuch nur die $K_\alpha$-Linien verwendet werden sollen, wird der Strahl
durch ein Material geleitet, dessen Absorptionskante zwischen den $K_\alpha$- und den
$K_\beta$-Linien liegt. Dadurch wirkt dieser Stoff wie ein $K_\beta$-Absorber,
der nur die gewünschten $K_\alpha$-Linien durchlässt.
Die $K_{\alpha,1}$-Linie mit $\lambda_1 = \SI{1,54093}{\angstrom}$ lässt sich
auf diese Weise nicht von der $K_{\alpha,2}$-Linie mit $\lambda_2 = \SI{1,54478}{\angstrom}$
trennen, wodurch es zu einer Ringaufspaltung kommen kann:
\begin{equation}
  \Delta \theta_{1,2} = \frac{\lambda_1 - \lambda_2}{\overline \lambda}\tan\theta
\end{equation}
$\overline\lambda$ ergibt sich mit den verschiedenen Emissionswahrscheinlichkeiten
zu $\overline\lambda = \SI{1,5417}{\angstrom}$
%------------------------------------------------
\section{Auswertung}
%------------------------------------------------
\subsection{Metallprobe}
Untersucht wird die Metallprobe mit der Nummer 6.
Um aus dem gemessenen Radius $r$ eines Reflexes den Winkel zu bestimmen, wird die Geometrie der Apparatur ausgenutzt.
Mit dem Umfang der Kamera $U=\SI{360}{\milli\meter}$ ergibt sich
\begin{equation}
	2\theta = \frac{2\pi}{U}r.
\end{equation}
Aus der Braggbedingung (\ref{eq:Bragg}) erhält man den Netzebenenabstand $d$.
Bis auf die letzten beiden Messwerte wurde mit der gemittelten Wellenlänge $\overline\lambda$ gerechnet.
Die Messwerte, sowie die berechneten Größen sind in Tabelle \ref{tbl:d} zu finden.
\tbl{d}

Um die Struktur zu bestimmen wird Gleichung (\ref{eq:d}) verwendet.
Hierbei wird das Verhältnis zum ersten Messwert gebildet, sodass sich die Gitterkonstante $a$ heraus kürzt.
Der Nenner aus den Millerschen Indices wird mit $m=\sqrt{h^2+k^2+l^2}$ bezeichnet.
Zu gegebenen Indices muss also die Gleichung
\begin{equation}
	\frac{d_1}{d_i} = \frac{m_i}{m_1}
\end{equation}
gelten.
Diese Werte sind in Tabelle \ref{tbl:fcc}, \ref{tbl:bcc} sowie \ref{tbl:diamant} dargestellt.
Es zeigt sich eine gute Übereinstimmung mit der fcc-Struktur.
\tbl{fcc}
\tbl{bcc}
\tbl{diamant}

Die Berechnete Gitterkonstante nach Gleichung (\ref{eq:d}) findet sich in Tabelle \ref{tbl:gitterkonstante},
zusammen mit den entsprechenden Reflexionswinkeln.
Nach Gleichung (\ref{eqn:fehler}) geht der Fehler mit $\cos^2\theta$.
Es wird also $a$ gegen diese Größe aufgetragen und mit linearer Regression und anschließender Extrapolation für $\theta = 0$ ein Ergebnis für die Gitterkonstante bestimmt.
Die Funktion
\begin{equation}
	a = c \cos^2(\theta) + b
\end{equation}
wird gefittet.
Die Parameter bestimmen sich zu
\begin{align*}
  c &= \val{c}\\
  b &= \val{b}
\end{align*}
Die Gitterkonstante der fcc-Struktur beträgt demnach $a = \val{b}$.

\tbl{gitterkonstante}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.9\textwidth]{build/gitterkonstante.pdf}
  \caption{Abhängigkeit der Gitterkonstante vom Streuwinkel.}
  \label{fig:gitterkonstante}
\end{figure}
\FloatBarrier


\subsection{Salzprobe}
Die Struktur der Salzprobe mit der Nummer 2 soll bestimmt werden.
Die Berechnung der Netzebenenabstände erfolgt analog zur Metallprobe.
Die Ergebnisse sind in Tabelle \ref{tbl:d_salz} dargestellt.
Für die letzten beiden Messwerte wurde auch hier der Unterschied der Wellenlängen berücksichtigt.
\tbl{d_salz}

Da es bei der vorliegenden Salzprobe möglich ist, dass schwache Reflexe nicht gemessen werden,
wird für jeden Messwert und für jede mögliche Kombination an Miller-Indices die Gitterkonstante $a$ berechnet.
Die resultierenden Ergebnisse sind in Tabelle \ref{tbl:cscl} sowie \ref{tbl:steinsalz} dargestellt.
\tbl{cscl}
\input{media/tbl_steinsalz}
Die Flourit- und Steinsalz-Struktur unterscheiden sich nur in der Ausprägung der Reflexe.
Gesucht ist nun eine gleich bleibende Gitterkonstante auf der Diagonalen.
Dabei können Reflexe übersprungen werden, wenn diese schwach sind und somit nicht gemessen wurden.
Die Flourit bzw. Steinsalz Tabelle zeigt ein solches Verhalten.
Die fehlenden Reflexe liegen genau bei den schwachen Reflexen der Flourit-Struktur,
sodass gefolgert werden kann, dass es sich um eben diese handelt.

Die Gitterkonstanten werden wieder gegen den $\cos^2\theta$ aufgetragen, um ein möglichst genaues Ergebnis für die Gitterkonstante zu erzielen.
Die lineare Ausgleichsrechnung liefert die Parameter
\begin{align*}
  c &= \val{c_salz}\\
  b &= \val{b_salz}.
\end{align*}
Das Salz besitzt eine Flourit-Struktur mit der Gitterkonstanten $a=\val{b_salz}$.
\tbl{gitterkonstante_salz}

\begin{figure}[tb]
  \centering
  \includegraphics[width=.9\textwidth]{build/gitterkonstante_salz.pdf}
  \caption{Abhängigkeit der Gitterkonstante vom Streuwinkel.}
  \label{fig:gitterkonstante_salz}
\end{figure}
\FloatBarrier

%------------------------------------------------
\section{Diskussion}
%------------------------------------------------
Die Strukturbestimmung mithilfe des Debey-Scherrer-Verfahrens liefert ein eindeutiges Ergebnis für die Struktur.
Die Metallprobe besitzt eine fcc-Struktur und eine Gitterkonstante von $\val{b}$.
Vergleicht man diesen Wert mit Literaturangaben \cite{database} handelt es sich wahrscheinlich um Aluminium,
mit einem Literaturwert von \SI{4.04958}{\angstrom}.

Die Salzprobe liegt in einer Flourit-Struktur vor. Die Gitterkonstante ist $\val{b_salz}$.
Diese liegt nahe dem Literaturwert für Calciumfluorid von \SI{5.4631}{\angstrom}

Die Messwerte in Abbildung \ref{fig:gitterkonstante} und \ref{fig:gitterkonstante_salz} zeigen einen Verlauf, der von einer Geraden abweicht.
In beiden Fällen ist die Krümmung gleich. Dies könnte an einer unzureichenden Beschreibung des systematischen Fehlers liegen,
oder durch einen systematischen Fehler beim Ablesen der Radien entstanden sein.

%------------------------------------------------
% LITERATURVERZEICHNIS
%------------------------------------------------
\begin{thebibliography}{xx}
	\small
  \bibitem{Anleitung}
    {\it Versuch \Versuchsnummer~- \Versuchstitel},
    \url{http://129.217.224.2/HOMEPAGE/Anleitung_FPMa.html},
    abgerufen am \Versuchsdatum.

  \bibitem{BradleyJay}
    A.J. Bradley,  A.H. Jay, Proc. Phys. Soc. {\bf 44} (1932) 563

  \bibitem{database}
  	{American Mineralogist Crystal Structure Database}",
  	\url{http://rruff.geo.arizona.edu/AMS/amcsd.php},
  	abgerufen am 26.06.2017

\end{thebibliography}

\end{document}
